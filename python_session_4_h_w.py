# -*- coding: utf-8 -*-
"""Python session_4_H.W.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1DKdoKMwU3ubWa0dWJPi7HB3iwJJwHo7d
"""

num = int(input("Enter a number: "))
factorial = 1

if num < 0:
   print("Sorry, factorial does not exist for negative numbers")
elif num == 0:
   print("The factorial of 0 is 1")
else:
   for i in range(1,num + 1):
       factorial = factorial*i
   print("The factorial of",num,"is",factorial)

for num in range(1500, 2701):
    if num % 7 == 0 and num % 5 == 0:
        print(num)

for i in range(1, 4):
    for j in range(i):
        print(i, end=' ')
    print()

# Get the input from the user
n = int(input("Enter the number of rows: "))

# Print the hollow inverted half pyramid
for i in range(n, 0, -1):
    for j in range(1, i+1):
        if i == n or j == 1 or j == i:
            # Print * if it's the first row or if it's the first or last column
            print("*", end="")
        else:
            # Print a space if it's not the first row and it's not the first or last column
            print(" ", end="")
    print()

num_rows = int(input("Enter the number of rows: "))

for i in range(1, num_rows+1):
    for j in range(1, num_rows-i+1):
        print(" ", end="")
    for j in range(1, 2*i):
        print("*", end="")
    print()

# Define a function to generate hollow full pyramid pattern
def hollow_full_pyramid(n):
    # Iterate through rows
    for i in range(1, n+1):
        # Print spaces for current row
        print(" "*(n-i), end="")
        
        # Print asterisks for current row
        if i == 1 or i == n:
            print("*"*(2*i-1), end="")
        else:
            print("*", end="")
            print(" "*(2*(i-1)-1), end="")
            print("*", end="")
        
        # Move to next row
        print()
        
# Test the function with n = 5
hollow_full_pyramid(5)